import os, logging, logging.handlers, time

def setLogging(archive, fh):
    
    PROGRAM_NAME      ="myBackup"
    LOG_FILENAME      = os.path.join(archive, PROGRAM_NAME + '.log')
    FILE_LOG_LEVEL    = logging.INFO   
    logger = logging.getLogger(PROGRAM_NAME)
    logger.setLevel(logging.INFO)    
    formatter = logging.Formatter('')    
    fh.setLevel(FILE_LOG_LEVEL)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def setfh(LOG_FILENAME):
    fh = logging.FileHandler(LOG_FILENAME)
    return fh
