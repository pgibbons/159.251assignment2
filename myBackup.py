import os, shutil, hashcode, pickle, log, time, re


def save(directory, myArchive = "myArchive"):    

    #Checks if the archive exists.
    if not os.path.exists(directory):
        print "No such directory, backup failed."
        return
    
    objectpath = os.path.join(myArchive, 'objects')   #Directory for files inside myArchive.
    indexpath = os.path.join(myArchive, "index.p")    #Path of the dictionary index.  
    index = {}                                        #Dictionary of paths and hashcodes.
    
    """Creates the archive if it isn't already there, if it needs to create the
    archive then obviously no index, therefore index is null. If the archive is
    already there tries to load a index, if it doesn't exist just continues.
    Also creates the object directory for the files inside myArchive."""
    
    if not os.path.exists(myArchive):
        os.mkdir(myArchive, 0777);
        os.mkdir(objectpath);
        newarchive = 1
    else:
        newarchive = 0
        try:            
            index = pickle.load(open(indexpath))
        except:            
            pass
    #Create logger:    
    fh = log.setfh(os.path.join(myArchive, 'myBackup.log'))
    logger = log.setLogging(myArchive, fh)
    
    if (newarchive == 1):
        # Add timestamp for first run.
        logger.info('---------\nBackup archive created on: %s.\n---------\n' % time.asctime())
    else:
        pass
       
    
    #Log and print messages.
    logger.info("\n**" + time.ctime() +" **")
    logger.info("The following archive was backed up: " + directory)
    logger.info("The following files were added:")
    print "The following files have been added to the archive: "

    """For every file in the named directory create the filepath of that file,
    then copy the file to the directory object in myArchive. Then creates
    the filepath of the copied file then renames the file as the SHA1 hash
    and stores the original filepath/filename and extension in the dictionary
    index (Key: filename, value: hash code). The renamed file is saved in the
    directory the python code is saved, so the renamed file is moved back in
    to the archive directory."""
    
    count = 0
    for dir, subdirs, files in os.walk(directory):        
        for f in files:
            filepath = os.path.join(dir, f)            
            shutil.copy(filepath, objectpath)
            path = os.path.join(objectpath, f)            
            hashcode = rename(path)
            index[filepath] = hashcode            
            try:
                shutil.move(hashcode, objectpath)
                print filepath
                logger.info(filepath)
                count =+ 1                
            except:
                os.unlink(hashcode)
    if count == 0:
        print "No new files were added."
        logger.info("No new files were added")
    #Saves the dictionary in the archive directory.        
    pickle.dump(index, open(indexpath, "wb"))
    logger.removeHandler(fh)
     

def rename(filepath):                
    filename = hashcode.createFileSignature(filepath)   #Generates SHA1 hashcode.   
    os.rename(filepath, filename)                       #Renames file.     
    return filename

def check(archiveD):                
    #Exceptions:
    
    #Correct directory.
    if not os.path.exists(archiveD): 
           print ("No such directory; " + archiveD + " Check failed")           
           return            
    else:
         pass
    #Directory has an index:    
    indexpath = os.path.join(archiveD, "index.p")
    try:
        index = pickle.load(open(indexpath))        
    except:
        print ("No index dictionary, check failed.")                
        return
    
    #Directory has a folder of objects:
    objectpath = os.path.join(archiveD, 'objects')
    if not os.path.exists(objectpath):
        print ("No object directory, check failed.")        
        return
    else:        
        pass
    #Create logger:
    fh = log.setfh(os.path.join(archiveD, 'myBackup.log'))
    logger = log.setLogging(archiveD, fh)    
    
    logger.info("\n" + time.asctime() + " Checking the archive.. ")
    
    count = 0                   #Correct files.
    incorrect = []              #Incorrect files.
    hashcodes = index.values()  #Hashcodes in the index.
    objects = []                #Files in object dir.
    notThere = []               #Files not in the directory.
    """
    For all the files in the archive generate the hash code
    and compare that with the file name, if equal increment
    the count of correct files, else add the incorrect file
    the list.
    """
    for dir, sibdirs, files in os.walk(objectpath):
        for f in files:
            filepath = os.path.join(objectpath, f)
            hashC = hashcode.createFileSignature(filepath)
            objects.append(hashC)
            if hashC == f:
                count += 1                
            else:
                incorrect.append(f)
    """
    For all the hashcodes in the index see if they are
    in the object directory, if not add it to a list.
    """
    for x in hashcodes:
        try:
            objects.index(x)
        except:
            notThere.append(x)
            
    #Print and log results.
    logger.info("Checking complete. \nResults:")
    print "Correct files: " + str(count)
    logger.info("Correct files: " + str(count))
    print "Incorrect files: "
    logger.info("Incorrect files: ")
    print incorrect
    logger.info(incorrect)
    print "There is an object for every object named in the index: "
    logger.info("There is an object for every object named in the index: ")
    if (len(notThere) == 0):
        print "True"
        logger.info("True")
    else:
        print "False"
        logger.info("False")
    logger.removeHandler(fh)

def List(archive, pattern = " "):
    
    #check to see if archive exists:
    try:
        indexpath = os.path.join(archive, "index.p")
        index = pickle.load(open(indexpath))
    except:
        print ("No archive, list failed")
        return
    
    keys = []
    keys = index.keys() #contents of the index in a list
    i = 0
    #check pattern and print contents in a list:
    if (pattern == " "):                        
                        for x in keys:
                            print x                         
    else:
        for x in keys:
            if re.match(pattern, x):
                print x
                i += 1
        if (i == 0):
            print ("No matches")
                

if __name__ == '__main__':
    import sys
    try:
        function = getattr(sys.modules[__name__], sys.argv[1])
        try:
            firstParam = sys.argv[2]
            try:
                secondParam = sys.argv[3]                
                function(firstParam, secondParam)
            except:       
                    function(firstParam)
        except:
            print "At least one parameter must be provided in the form:"
            print "python myBackup.py functiom parameter"
    except:
        print "Must be in the form: python myBackup.py function parameter"
   
