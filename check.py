import os, shutil, hashcode, pickle, log, time

def check(archiveD = 'myArchive'):                
    #Exceptions:
    
    #Correct directory.
    if not os.path.exists(archiveD): 
           print ("No such directory; " + archiveD + " Check failed")           
           return            
    else:
         pass
    #Directory has an index:    
    indexpath = os.path.join(archiveD, "index.p")
    try:
        index = pickle.load(open(indexpath))
    except:
        print ("No index dictionary, check failed.")                
        return
    
    #Directory has a folder of objects:
    objectpath = os.path.join(archiveD, 'objects')
    if not os.path.exists(objectpath):
        print ("No object directory, check failed.")        
        return
    else:        
        pass

    #Create logger:    
    fh = log.setfh(os.path.join(myArchive, 'myBackup.log'))
    logger = log.setLogging(myArchive, fh)
       
    logger.info("\n" + time.asctime() + " Checking the archive.. ")
    
    count = 0                   #Correct files.
    incorrect = []              #Incorrect files.
    hashcodes = index.values()  #Hashcodes in the index.
    objects = []                #Files in object dir.
    notThere = []               #Files not in the directory.
    """
    For all the files in the archive generate the hash code
    and compare that with the file name, if equal increment
    the count of correct files, else add the incorrect file
    the list.
    """
    for dir, sibdirs, files in os.walk(objectpath):
        for f in files:
            filepath = os.path.join(objectpath, f)
            hashC = hashcode.createFileSignature(filepath)
            objects.append(hashC)
            if hashC == f:
                count += 1                
            else:
                incorrect.append(f)
    """
    For all the hashcodes in the index see if they are
    in the object directory, if not add it to a list.
    """
    for x in hashcodes:
        try:
            objects.index(x)
        except:
            notThere.append(x)    
    logger.info("Checking complete. \nResults:")
    print "Correct files: " + str(count)
    logger.info("Correct files: " + str(count))
    print "Incorrect files: "
    logger.info("Incorrect files: ")
    print incorrect
    logger.info(incorrect)
    print "There is an object for every object named in the index: "
    logger.info("There is an object for every object named in the index: ")
    if (len(notThere) == 0):
        print "True"
        logger.info("True")
    else:
        print "False"
        logger.info("False")    
    logger.removeHandler(fh)
