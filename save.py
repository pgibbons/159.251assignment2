import os, shutil, hashcode, pickle, log, time


def save(directory, myArchive = "myArchive"):

    #Checks if the archive exists.
    if not os.path.exists(directory):
        print "No such directory, backup failed."
        return
    
    objectpath = os.path.join(myArchive, 'objects')   #Directory for files inside myArchive.
    indexpath = os.path.join(myArchive, "index.p")    #Path of the dictionary index.  
    index = {}                                        #Dictionary of paths and hashcodes.
    
    """Creates the archive if it isn't already there, if it needs to create the
    archive then obviously no index, therefore index is null. If the archive is
    already there tries to load a index, if it doesn't exist just continues.
    Also creates the object directory for the files inside myArchive."""    
    
    if not os.path.exists(myArchive):
        os.mkdir(myArchive, 0777);
        os.mkdir(objectpath);
        newarchive = 1
    else:
        newarchive = 0
        try:            
            index = pickle.load(open(indexpath))
        except:
            pass
        
    #Create logger:    
    fh = log.setfh(os.path.join(myArchive, 'myBackup.log'))
    logger = log.setLogging(myArchive, fh)
       
    if (newarchive == 1):
        # Add timestamp for first run.
        logger.info('---------\nBackup archive created on: %s.\n---------\n' % time.asctime())
    else:
        pass      
    
    #Log and print messages.
    logger.info("\n**" + time.ctime() +" **")
    logger.info("The following archive was backed up: " + directory)
    logger.info("The following files were added:")
    print "The following files have been added to the archive: "

    """For every file in the named directory create the filepath of that file,
    then copy the file to the directory object in myArchive. Then creates
    the filepath of the copied file then renames the file as the SHA1 hash
    and stores the original filepath/filename and extension in the dictionary
    index (Key: filename, value: hash code). The renamed file is saved in the
    directory the python code is saved, so the renamed file is moved back in
    to the archive directory."""
    
    count = 0
    for dir, subdirs, files in os.walk(directory):        
        for f in files:
            filepath = os.path.join(dir, f)            
            shutil.copy(filepath, objectpath)
            path = os.path.join(objectpath, f)            
            hashcode = rename(path)
            index[filepath] = hashcode
            print filepath
            try:
                shutil.move(hashcode, objectpath)                
                logger.info(filepath)
                count =+ 1                
            except:
                os.unlink(hashcode)
    if count == 0:
        print "No new files were added."
        logger.info("No new files were added")
    #Saves the dictionary in the archive directory.        
    pickle.dump(index, open(indexpath, "wb"))
    #Remove handler.
    logger.removeHandler(fh)   

def rename(filepath):                
    filename = hashcode.createFileSignature(filepath)   #Generates SHA1 hashcode.   
    os.rename(filepath, filename)                       #Renames file.     
    return filename                                 
